<?php

/**
 * @property string $id
 * @property string $template
 * @property array $children
 * @property array $data
 * @property string $output
 * @property Loader $load
 * @property SsoUser $user
 * @property Url $url
 * @property Log $log
 * @property Request $request
 * @property Response $response
 * @property Cache $cache
 * @property Session $session
 * @property Language $language
 * @property Document $document
 * @property Length $length
 * @property Cart $cart
 * @property Encryption $encryption
 * @property Event $event
 * @property Config $config
 * @property Layout $layout
 * @property ModelMemberRegister $model_member_register
 * @property ModelSettingSetting $model_setting_setting
 * @property ModelToolImage $model_tool_image
 * @property ModelToolOnline $model_tool_online
 * @property ModelCalenderCalender $model_calender_calender
 * @property ModelUserProfile $model_user_profile
 * @property ModelManagerFaq $model_manager_faq
 * @property ModelManagerFiles $model_manager_files
 * @property ModelContactusContactus $model_contactus_contactus
 * @property ModelUploadingUploading $model_uploading_uploading
 * @property ModelUserLog $model_user_log
 * @property ModelDownloadingDownloading $model_downloading_downloading
 * @property ModelArchiveArchive $model_archive_archive
 * @property ModelFaqHome $model_faq_home
 * @property ModelMemberVendorCategory $model_member_vendor_category
 * @property   ModelUserUser $model_user_user
 * @property ModelCustomersCustomers $model_customers_customers
 * @property Userlog $userLog
 * @property PersianDate $persianDate
 **/
abstract class Controller {
    protected $registry;

    public function __construct($registry) {
        $this->registry = $registry;
    }

    public function __get($key) {
        return $this->registry->get($key);
    }

    public function __set($key, $value) {
        $this->registry->set($key, $value);
    }
}