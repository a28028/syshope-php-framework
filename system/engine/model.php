<?php
/**
 * @property string $id
 * @property string $template
 * @property array $children
 * @property array $data
 * @property string $output
 * @property Loader $load
 * @property DB $db
 * @property DB $db_mssql
 * @property DB $dbb
 * @property SsoUser $user
 * @property Url $url
 * @property Log $log
 * @property Request $request
 * @property Response $response
 * @property Cache $cache
 * @property Session $session
 * @property Document $document
 * @property Customer $customer
 * @property Affiliate $affiliate
 * @property Encryption $encryption
 * @property Event $event
 * @property Userlog $userLog
 * @property ModelExtensionEvent $model_extension_event
 * @property ModelExtensionExtension $model_extension_extension
 * @property ModelExtensionModification $model_extension_modification
 * @property ModelMenuTree $model_menu_tree
 * @property ModelMenuWidget $model_menu_widget
 * @property ModelSettingSetting $model_setting_setting
 * @property ModelTellInbox $model_tell_inbox
 * @property ModelTellReport $model_tell_report
 * @property ModelToolBackup $model_tool_backup
 * @property ModelToolImage $model_tool_image
 * @property ModelToolUpload $model_tool_upload
 * @property ModelUserApi $model_user_api
 * @property ModelUserUser $model_user_user
 *  @property ModelUserProfile $model_user_profile
 * @property ModelUserUserGroup $model_user_user_group
 **/
abstract class Model {
	protected $registry;

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
}