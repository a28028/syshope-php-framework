<?php

final class Loader
{
    private $registry;

    public function __construct($registry)
    {
        $this->registry = $registry;
    }

    public function controller($route, $args = array())
    {
        $action = new Action($route, $args);

        return $action->execute($this->registry);
    }

    public function model($model)
    {
        $file = DIR_APPLICATION . 'model/' . $model . '.php';
        $class = 'Model' . preg_replace('/[^a-zA-Z0-9]/', '', $model);

        if (file_exists($file)) {
            include_once(modification($file));

            $this->registry->set('model_' . str_replace('/', '_', $model), new $class($this->registry));
        } else {
            //debug_print_backtrace();
            trigger_error('Error: Could not load model ' . $file . '!');
            exit();
        }
    }

    public function view($templatePath, $data = array())
    {
        $extension = pathinfo($templatePath)['extension'];
        switch ($extension){
            case  'tpl':
                $data['url'] = $this->registry->get('url');
                //$this->model('tool/file');
                $data['fileUrl'] = new FileUrl($this->registry);
                $template = new Template('template');
                foreach ($data as $key => $value) {
                    $template->set($key, $value);
                }
                $output = $template->render($templatePath);
                return $output;
                break;
            case  'twig':
                $template = new Template('twig');
                foreach ($data as $key => $value) {
                    $template->set($key, $value);
                }
                //remove extension from templatePath
                $templatePath = preg_replace('/\\.[^.\\s]{3,4}$/', '', $templatePath);
                $output = $template->render($templatePath);
                return $output;
                break;
            default :
                $file = DIR_TEMPLATE . $templatePath;
                trigger_error('Error: Could not exists template engine for   {$extension} extension template ' . $file . '!');
                exit();
        }
    }

    //add ne hossein rajaee
    public function angView($template, $data = array())
    {
        $file = DIR_ANG_TEMPLATE . $template;
        if (file_exists($file)) {
            extract($data);

            ob_start();

            require(modification($file));

            $output = ob_get_contents();

            ob_end_clean();

            return $output;
        } else {
            trigger_error('Error: Could not load template ' . $file . '!');
            exit();
        }
    }

    public function library($library)
    {
        $file = DIR_SYSTEM . 'library/' . $library . '.php';

        if (file_exists($file)) {
            include_once(modification($file));
        } else {
            trigger_error('Error: Could not load library ' . $file . '!');
            exit();
        }
    }

    public function helper($helper)
    {
        $file = DIR_SYSTEM . 'helper/' . $helper . '.php';

        if (file_exists($file)) {
            include_once(modification($file));
        } else {
            trigger_error('Error: Could not load helper ' . $file . '!');
            exit();
        }
    }

    public function config($config)
    {
        $this->registry->get('config')->load($config);
    }

    public function language($language)
    {
        debug_print_backtrace();
        return $this->registry->get('language')->load($language);
    }
}