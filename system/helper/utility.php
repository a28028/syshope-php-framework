<?php
if (!function_exists('file_get_contents_url')) {
    function file_get_contents_url($url, $use_include_path = false, $context = false)
    {
        return file_get_contents($url);
        if (!array_key_exists('REMOTE_ADDR', $_SERVER) || $_SERVER['REMOTE_ADDR'] != '172.18.84.148') {
            return file_get_contents($url);
        }
        if ($context == false) {
            $sessionName = ini_get('session.name');
            $session_id = session_id();
            $opts = array(
                'http' => array(
                    'method' => "GET",
                    'header' => "Accept-language: en\r\n" .
                        "Cookie: {$sessionName}={$session_id};\r\n"
                )
            );
            $context = stream_context_create($opts);
            return file_get_contents($url, false, $context);
        }
        return file_get_contents($url);
    }

}