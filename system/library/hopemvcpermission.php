<?php

/**
 * Created by hope.
 * User: hossein rajaee
 * Date: 8/1/15
 * Time: 11:30 AM
 */
class HopeMvcPermission
{
    private $dirApplication = array();
    private $ignore = array(
        'common/dashboard',
        'common/startup',
        'common/login',
        'common/logout',
        'common/forgotten',
        'common/reset',
        'error/not_found',
        'error/permission',
        'common/footer',
        'common/header'
    );

    public function __construct($arrayDirApplication, $ignore = [])
    {
        $this->dirApplication = $arrayDirApplication;
        foreach ($ignore as $item) {
            $this->ignore[] = $item;
        }
    }

    public function getListItemPermission()
    {
        $permissions = array();
        foreach ($this->dirApplication as $dir) {
            $files = glob($dir . 'controller/*/*.php');
            foreach ($files as $file) {
                $part = explode('/', dirname($file));
                $permission = end($part) . '/' . basename($file, '.php');
                if (!in_array($permission, $this->ignore)) {
                    // $permissions[] = $permission;
                    $methods = $this->getClassMethodsController($file);
                    $item = new stdClass();
                    $item->key = $permission;
                    $item->keyGroup = $permission;
                    $item->comment = $methods['classComment'];
                    $permissions[] = $item;
                    foreach ($methods['methods'] as $method) {
                        $item = new stdClass();
                        $item->keyGroup = $permission;
                        $item->key = $permission . '/' . $method->name;//strtolower($method);
                        $item->comment = $method->comment;
                        $permissions[] = $item;//$permission . '/' . $method->name;
                    }
                }
            }
        }
        return $permissions;
    }

    private function getClassMethodsController($file)
    {
        $part = explode('/', dirname($file));
        $basename = basename($file, '.php');
        //column_left To ColumnLeft
        $basenameArray = explode('_', $basename);
        $basename = '';
        foreach ($basenameArray as $item) {
            $basename .= ucfirst($item);
        }
        //ControllerCommonColumnLeft
        //column_left
        //echo 'Controller'. ucfirst(end($part)).$basename;exit;
        $class_name = 'Controller' . ucfirst(end($part)) . $basename;

        include_once($file);
        if(! class_exists($class_name)){
            $message = "Class $class_name not found in".$file."\n";
            $message.=$file." => ". "(راهنمایی فایل را یا حذف یا اصلاح نمایید)"."\n";
            throw new Exception($message);
         //  echo  "Class $class_name not found in".$file;
            exit;
        }
        //extends RestServer
        if (is_subclass_of($class_name, 'RestServer')) {
            $methods = get_class_methods($class_name);
        }
        else if (is_subclass_of($class_name, 'RestPerSystem')) {
            $methods = get_class_methods($class_name);
        }
        else{
            $methods = get_class_methods(new  $class_name(''));
        }
        $methods = get_class_methods($class_name);
        $reflector = new ReflectionClass($class_name);
        $classComment = $this->getComment($reflector);
        $ignorMethods = array('__construct', '__get', '__set');
        $result = array();
        $result['classComment'] = $classComment;
        if($classComment &&  strlen($classComment) > 1){
            $classComment =  $classComment; 
        }
        $result['methods'] = [];
        foreach ($methods as $method) {
            if (!in_array($method, $ignorMethods)) {
                $refMetode = $reflector->getMethod($method);
                $item = new stdClass();
                $item->name = strtolower($method);
                   /* //ReflectionMethod
                    if (!$refMetode instanceof getCommentMetode) {
                        echo $file .' => '.$class_name .' => '.$method ;exit;
                    }*/
                $comment = $this->getCommentMetode($refMetode);
                if($classComment &&  strlen($classComment) > 1){
                    $sep = strlen($comment) > 0 ? "=>" :"";
                   // $comment =  $classComment.$sep.$comment;
                }
                $item->comment = $comment;
                $result['methods'] [] = $item;//strtolower($method);
            }
        }
        return $result;
    }

    private function startsWith($haystack, $needle)
    {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    private function getComment(ReflectionClass $reflector)
    {
        $comment = $reflector->getDocComment();
        foreach (explode("\n", $comment) as $line) {
            //echo $line."</br>";
            $line = trim($line);
            if ($this->startsWith(strtolower($line), '*@desc') || $this->startsWith(strtolower($line), '* @desc')) {
                $line = str_ireplace('*@desc' ,'', $line );
                $line = str_ireplace('*@Desc' ,'', $line );
                $line = str_ireplace('* @desc' ,'', $line );
                $line = str_ireplace('* @Desc' ,'', $line );
                return $line;
            }
            // do something
        }
        return '';
    }
    private function getCommentMetode(ReflectionMethod $reflector)
    {
        $comment = $reflector->getDocComment();
        foreach (explode("\n", $comment) as $line) {
            //echo $line."</br>";
            $line = trim($line);
            if ($this->startsWith(strtolower($line), '*@desc') || $this->startsWith(strtolower($line), '* @desc')) {
                $line = str_ireplace('*@desc' ,'', $line );
                $line = str_ireplace('*@Desc' ,'', $line );
                $line = str_ireplace('* @desc' ,'', $line );
                $line = str_ireplace('* @Desc' ,'', $line );
                return $line;
            }
            // do something
        }
        return '';
    }

    

}