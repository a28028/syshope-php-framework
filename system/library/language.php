<?php
class Language {
	private $default = 'english';
	private $directory;
	private $data = array();
	private $languages = array();

	public function __construct($directory = '', $languages) {
		$this->directory = $directory;
		$this->languages = $languages;
	}

	public function get($key) {
		return (isset($this->data[$key]) ? $this->data[$key] : $key);
	}
	
	public function load($filename) {
		$_ = array();

		$file = DIR_LANGUAGE . $this->default . '/' . $filename . '.php';

		if (file_exists($file)) {
			require($file);
		}

		$file = DIR_LANGUAGE . $this->directory . '/' . $filename . '.php';

		if (file_exists($file)) {
			require($file);
		}

		$this->data = array_merge($this->data, $_);

		return $this->data;
	}
	public function getLanguages(){
	    return $this->languages;
    }
}