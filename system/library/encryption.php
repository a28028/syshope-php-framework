<?php
final class Encryption {
	private $key;

	public function __construct($key) {
		$this->key = hash('sha256', $key, true);
	}

	public function encrypt($value) {
		if(!function_exists('mcrypt_encrypt')){
			return $value;
		}
		return strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, hash('sha256', $this->key, true), $value, MCRYPT_MODE_ECB)), '+/=', '-_,');
	}

	public function decrypt($value) {
		if(!function_exists('mcrypt_encrypt')){
			return $value;
		}
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, hash('sha256', $this->key, true), base64_decode(strtr($value, '-_,', '+/=')), MCRYPT_MODE_ECB));
	}

    public function encryptWithDelimiter($value, $delimiter = "_" , $rand = false)
    {
        /* if (!isset($this->session->data['encryptDelimiterListIndex'])) {
             $this->session->data['encryptDelimiterListIndex'] = "_{" . rand(10, 100) . "}_";
         }
         $delimiter = $this->session->data['encryptDelimiterListIndex'];*/
        if($rand == false){
            return $this->encrypt($value . $delimiter . rand(1000, 9999));
        }else{
            return $this->encrypt($value . $delimiter .$rand);
        }

    }

    public function decryptWithDelimiter($value, $delimiter = "_")
    {
       // $delimiter = "_";//$this->session->data['encryptDelimiterListIndex'];
        $value = $this->decrypt($value);
        $value = explode($delimiter, $value);
        $value = $value[0];
        return $value;
    }

}

