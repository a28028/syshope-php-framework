<?php

/**
 * Created by PhpStorm.
 * User: hossein
 * Date: 4/30/17
 * Time: 9:42 AM
 */
class HopeControl {

    public function __construct($request ,$config_limit_admin){
        $this->_config_limit_admin = $config_limit_admin;
        $this->_request = $request;
    }
    public function getFilter_option() {
        $request = $this->_request;
        $filter_option = array();
        if (isset($request['sort'])) {
            $filter_option['sort'] = $request['sort'];
        } else {
            $filter_option['sort'] = '';
        }
        if (isset($request['order'])) {
            $filter_option['order'] = strtoupper($request['order']);
        } else {
            $filter_option['order'] = 'DESC';
        }
        if (isset($request['page'])) {
            $filter_option['page'] = $request['page'];
        } else {
            $filter_option['page'] = 1;
        }
        $filter_option['start'] = ($filter_option['page'] - 1) * $this->_config_limit_admin;
        $filter_option['limit'] = $this->_config_limit_admin;
        if (isset($filter_option['start']) || isset($filter_option['limit'])) {
            if ($filter_option['start'] < 0) {
                $filter_option['start'] = 0;
            }
            if ($filter_option['limit'] < 1) {
                $filter_option['limit'] =  $this->_config_limit_admin;
            }
        }
        return $filter_option;
    }

    public function getPagination($dataModel) {
        $total = $dataModel['total'];
        $filter_option = $this->getfilter_option();
        $pagination = new stdClass();
        $pagination->total = $total;
        $pagination->currentPage = $filter_option['page'];
        $pagination->pageSize = $this->_config_limit_admin;
        $dataModel['pagination'] = $pagination;
        $dataModel['sort'] = $filter_option['sort'];
        $dataModel['order'] = $filter_option['order'];
        return $dataModel;
    }
}