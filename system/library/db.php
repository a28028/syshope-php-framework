<?php

class DB {
    private $db;

    public function __construct($driver, $hostname, $username, $password, $database) {
        $class = 'DB\\' . $driver;
        if (class_exists($class)) {
            $this->db = new $class($hostname, $username, $password, $database);
        } else {
            exit('Error: Could not load database driver ' . $driver . '!');
        }
    }

    /**
     * @param $sql
     * @return db_queryResult
     */
    public function query($sql) {
        return $this->db->query($sql);
    }

    public function escape($value) {
        return $this->db->escape($value);
    }

    public function countAffected() {
        return $this->db->countAffected();
    }

    public function getLastId() {
        return $this->db->getLastId();
    }

    /**
     * @param $tableName
     * @return mixed
     */
    public function getTable($tableName){
        if(method_exists($this->db , "getTable")){
            return  $this->db->getTable($tableName);
        }
        else{
            return $tableName;
        }

    }

    /**
     * @param $key
     * @param $value
     * set Property Value Db
     */
    public function setAdditionalProperty($key, $value) {
        $methodName = "set_{$key}";
        if (method_exists($this->db, $methodName)) {
            $this->db->$methodName($value);
        }
    }

    public function message($message) {
        header('Content-Type: application/json');
        return json_encode($message);
    }

    /**/
    public function _insert($table, $keys, $values) {
        if (method_exists($this->db, "_insert")) {
            return $this->db->_insert($table, $keys, $values);
        } else {
            return 'INSERT INTO ' . $table . ' (' . implode(', ', $keys) . ') VALUES (' . implode(', ', $values) . ')';
        }
    }

    public function _limit($sql, $qb_limit = false, $qb_offset = false) {
        if (method_exists($this->db, "_limit")) {
            return $this->db->_limit($sql, $qb_limit, $qb_offset);
        } else {
            return $sql . ' LIMIT ' . ($qb_offset ? $qb_offset . ', ' : '') . (int)$qb_limit;
        }
    }
}

class db_queryResult {
    public $row = array();
    public $rows = array();
    public $num_rows = 0;
}
 