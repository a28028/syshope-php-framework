<?php

/**
 * User: hpuser
 * Time: 04:10 PM
 */
class Layout
{
    private $registry;
    private $defaultlayoutRoute = 'common/default_layout';

    public function __construct($registry)
    {
        $this->registry = $registry;
    }

    public function __get($key)
    {
        return $this->registry->get($key);
    }

    public function __set($key, $value)
    {
        $this->registry->set($key, $value);
    }

    public function setDefaultLayoutRoute($route)
    {
        $this->defaultlayoutRoute = $route;
    }

    public function showContent($contentController ,$layoutRoute = '')
    {
        if(strlen($layoutRoute) == 0){
            $layoutRoute = $this->defaultlayoutRoute;
        }
        $this->load->controller($layoutRoute, array('contentController' => $contentController));
    }


}