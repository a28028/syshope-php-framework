<?php
if( array_key_exists('REMOTE_ADDR', $_REQUEST )  && $_REQUEST['REMOTE_ADDR'] == '172.18.84.148' || true){

    class Session
    {
        public $data = array();
        private $session_start;

        public function __construct($session_start = true)
        {
            $this->session_start = $session_start;
            if (!session_id()) {
                ini_set("session.gc_maxlifetime", "3600");
                ini_set('session.use_only_cookies', 'On');
                ini_set('session.use_trans_sid', 'Off');
                ini_set('session.cookie_httponly', 'Off');
                $save_handler = ini_get('session.save_handler');
                session_set_cookie_params(0, '/');
                if ($this->session_start) {
                    session_start();
                }
                $this->data =& $_SESSION;
            }
        }

        public function getId()
        {
            if ($this->session_start)
                return session_id();
            else
                return '';
        }

        public function destroy()
        {
            if ($this->session_start)
                return session_destroy();
        }
    }
}
else{
class Session {
    public $data = array();

    public function __construct() {
        if (!session_id()) {
            ini_set('session.use_only_cookies', 'On');
            ini_set('session.use_trans_sid', 'Off');
            ini_set('session.cookie_httponly', 'On');
            session_set_cookie_params(0, '/');
            session_start();
        }

        $this->data =& $_SESSION;
    }

    public function getId() {
        return session_id();
    }

    public function destroy() {
        return session_destroy();
    }
}
}
