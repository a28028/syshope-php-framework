<?php
class Document {
	private $title;
	private $description;
	private $keywords;
	private $links = array();
	private $styles = array();
	private $scripts = array();

	public function setTitle($title) {
		$this->title = $title;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setKeywords($keywords) {
		$this->keywords = $keywords;
	}

	public function getKeywords() {
		return $this->keywords;
	}

	public function addLink($href, $rel) {
		$this->links[$href] = array(
			'href' => $href,
			'rel'  => $rel
		);
	}

	public function getLinks() {
		return $this->links;
	}

	public function addStyle($href, $rel = 'stylesheet', $media = 'screen') {
		$this->styles[$href] = array(
			'href'  => $href,
			'rel'   => $rel,
			'media' => $media
		);
	}

	public function getStyles() {
		return $this->styles;
	}

	public function addScript($script) {
		$this->scripts[md5($script)] = $script;
	}

	public function getScripts() {
		return $this->scripts;
	}

    /**
     *  @deprecated default_layout.php
     */
    public function loadAngular(){
        $this->addScript('view/resource/global.string.js');
        $this->addScript('view/resource/angular/angular.js');
		if(defined('HTTP_SYSTEM_UI')){
			$this->addScript(HTTP_SYSTEM_UI."lib/angular/angular-ocLazyLoad/ocLazyLoad.js");
		}
		$this->addScript('view/resource/angular/angular-animate/angular-animate.js');
        $this->addScript('view/resource/angular/angular-animate/angular-animate.js');
        $this->addScript('view/resource/angular/angular-ui-bootstrap/ui-bootstrap.js');
        $this->addScript('view/resource/angular/angular-toaster/toaster.js');


        $this->addScript('view/resource/angular/ng-file-upload/ng-file-upload.min.js');
        $this->addScript('view/resource/angular/ng-file-upload/ng-file-upload-shim.min.js');
        $this->addScript('view/resource/angular/angular-loading-bar/loading-bar.min.js');
        $this->addScript('view/resource/angular/angular-confirm/angular-confirm.js');
        

        $this->addStyle('view/resource/angular/angular-loading-bar/loading-bar.min.css');
        $this->addStyle('view/resource/angular/angular-toaster/toaster.min.css');

		$this->addStyle('view/resource/angular/angular-chart/angular-chart.min.css');
		$this->addScript('view/resource/angular/angular-chart/Chart.min.js');
		$this->addScript('view/resource/angular/angular-chart/angular-chart.min.js');

		$this->addScript('view/resource/angular/angular-daterangepicker/moment.js');
		$this->addScript('view/resource/angular/angular-daterangepicker/moment-jalali.js');
/*		$this->addStyle('view/resource/angular/angular-barcode/barcode.css');
		$this->addScript('view/resource/angular/angular-barcode/barcode.js');*/


		$this->addScript('view/resource/angular/angular-sanitize/angular-sanitize.js');

		$this->addStyle('view/resource/angular/autocomplete/autocomplete.css');
		$this->addScript('view/resource/angular/autocomplete/autocomplete.js');

		$this->addScript('view/resource/angular/angular-input-stars/angular-input-stars.js');
		$this->addStyle('view/resource/angular/angular-input-stars/angular-input-stars.css');

		$this->addScript('view/resource/angular/angular-device-detector/re-tree.js');
		$this->addScript('view/resource/angular/angular-device-detector/ng-device-detector.js');

		$this->addScript('view/resource/angular/myApp.js');
		if(defined('HTTP_SYSTEM_UI')){
			$this->addScript(HTTP_SYSTEM_UI."app/config.lazyload.js");
		}
		$this->addScript('view/resource/angular/factory/services.js');

    }

}