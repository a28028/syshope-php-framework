<?php
class Request {
	public $get = array();
	public $post = array();
	public $cookie = array();
	public $files = array();
	public $server = array();

	public function __construct() {
		// حذف   موارد هنگامی که json  پست شده است
		if(count($_POST) == 1){
			reset($_POST);
			$first_key = key($_POST);
			// is json  post
			if(json_decode($first_key)){
				unset($_POST[$first_key]);
			}
			end($_REQUEST);
			$last_key = key($_REQUEST);
			if(json_decode($last_key)){
				unset($_REQUEST[$last_key]);
			}
		}
		$_JSON = json_decode(file_get_contents('php://input'), true);
		if(isset($_JSON) && is_array($_JSON)){
			$_POST = $_POST + $_JSON;
			$_REQUEST = $_REQUEST + $_JSON;
		}
		$this->get = $this->clean($_GET);
		$this->post = $this->clean($_POST);
		$this->request = $this->clean($_REQUEST);
		$this->cookie = $this->clean($_COOKIE);
		$this->files = $this->clean($_FILES);
		$this->server = $this->clean($_SERVER);
	}

	/*public function clean($data) {
		if (is_array($data)) {
			foreach ($data as $key => $value) {
				unset($data[$key]);

				$data[$this->clean($key)] = $this->clean($value);
			}
		} else {
			$data = htmlspecialchars($data, ENT_COMPAT, 'UTF-8');
		}

		return $data;
	}*/
	public function clean($data) {
		if (is_array($data)) {
			foreach ($data as $key => $value) {
				unset($data[$key]);
				$data[$this->clean($key)] = $this->clean($value);
			}
		} else {
			$data = htmlspecialchars($data, ENT_COMPAT, 'UTF-8');
			$data = str_replace(array('ي','ك') ,array('ی','ک'), $data);
		/*	$data = str_replace('ي', 'ی', $data);
			$data = str_replace('ك', 'ک', $data);*/
		}

		return $data;
	}
}