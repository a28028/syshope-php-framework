<?php

namespace DB;
final class SQLite
{
    private $link;

    public function __construct($hostname, $username, $password, $database)
    {
        $this->link = new \SQLite3($hostname);
        if (!$this->link) {
            trigger_error('Error: Could not make a database link using ' . $username . '@' . $hostname);
        }
    }

    public function query($sql)
    {
        //SQLite3Result
        if (stripos($sql, "select") == false && stripos($sql, "from") == false) {
            if (!$this->link->exec($sql)) {
                trigger_error('Error: ' . $this->link->lastErrorMsg() . '<br />Error No: ' . $this->link->lastErrorCode() . '<br />' . $sql);
                exit();
            }
            return true;
        } else {
            $SQLite3Result = $this->link->query($sql);
            if ($SQLite3Result) {
                $i = 0;
                $data = array();
                while ($result = $SQLite3Result->fetchArray(SQLITE3_ASSOC)) {
                    $data[$i] = $result;

                    $i++;
                }
                $query = new \stdClass();
                $query->row = isset($data[0]) ? $data[0] : array();
                $query->rows = $data;
                $query->num_rows = $i;

                unset($data);
                return $query;

            } else {
                trigger_error('Error: ' . $this->link->lastErrorMsg() . '<br />Error No: ' . $this->link->lastErrorCode() . '<br />' . $sql);
                exit();
            }
        }

    }

    public function escape($value)
    {
        return $this->link->escapeString($value);
    }

    public function countAffected()
    {
        return mysql_affected_rows($this->link);
    }

    public function getLastId()
    {
        return $this->link->lastInsertRowID();
    }

    public function __destruct()
    {
        $this->link->close();
    }
}

?>