<?php

namespace DB;
final class Postgre {
    private $link;
    private $argument;
    private $connectionStatus = false;

    public function __construct($hostname, $username, $password, $database, $isNowOpenConnection = false) {
        // if($_SERVER['REMOTE_ADDR'] =='172.18.84.148'){
        $this->argument =  array(
            'hostname' => $hostname,
            'username' => $username,
            'password' => $password,
            'database' => $database
        );
        if($isNowOpenConnection == true){
            $this->openConnection();
        }

    }
    public function openConnection(){
        if(!$this->connectionStatus){
            /**
             * @var $hostname
             * @var $username
             * @var $password
             * @var $database
             */
            extract($this->argument);
            $conn_string = 'host=' . $hostname . ' port=5432 user=' . $username . ' password='	. $password . ' dbname=' . $database;
            if (!$this->link = pg_connect($conn_string)   ) {
                trigger_error('Error: Could not make a database link using ' . $username . '@' . $hostname);
            }
            pg_query($this->link, "SET CLIENT_ENCODING TO 'UTF8'");
            $this->connectionStatus = true;
        }

    }

    public function query($sql) {
        $this->openConnection();
        //  echo $sql."</br>\n";
        $resource = pg_query($this->link, $sql);

        if ($resource) {
            if (is_resource($resource)) {
                $i = 0;

                $data = array();

                while ($result = pg_fetch_assoc($resource)) {
                    $data[$i] = $result;

                    $i++;
                }

                pg_free_result($resource);

                $query = new \stdClass();
                $query->row = isset($data[0]) ? $data[0] : array();
                $query->rows = $data;
                $query->num_rows = $i;

                unset($data);

                return $query;
            } else {
                return true;
            }
        } else {
            trigger_error('Error: ' . pg_result_error($this->link) . '<br />' . $sql);
            exit();
        }
    }

    public function escape($value) {
        $this->openConnection();
        return pg_escape_string($this->link, $value);
    }

    public function countAffected() {
        $this->openConnection();
        return pg_affected_rows($this->link);
    }

    public function getLastId() {
        $this->openConnection();
        $query = $this->query("SELECT LASTVAL() AS `id`");

        return $query->row['id'];
    }

    public function getTable($tableName){
        $DB_PREFIX = DB_PREFIX;
        $DB_SCHEMA = DB_SCHEMA;
        if(isset( $this->_schema) && strlen($this->_schema) > 0){
            $DB_SCHEMA =  $this->_schema;
        }
        if(isset( $this->_prefix) && strlen($this->_prefix) > 0){
            $DB_PREFIX =  $this->_prefix;
        }
        return  $DB_SCHEMA .".".$DB_PREFIX . $tableName;
    }

    public function set_schema($value){
        $this->_schema = $value;
    }
    public function set_perfix($value){
        $this->_prefix = $value;
    }

    public function _limit($sql, $qb_limit = false, $qb_offset = false) {
        if ($qb_limit) {
            $sql .= " LIMIT " . (int)$qb_limit;
            if ($qb_offset) {
                $sql .= " OFFSET " . $qb_offset;
            }
            return $sql;
        }
        return $sql;
    }

    public function __destruct() {
        /* $arrayIpTest = '172.18.84.148';
         $arrayIpTest = explode(",",$arrayIpTest);
         if(in_array(trim($_SERVER['REMOTE_ADDR']) ,$arrayIpTest)){
             echo '__destruct';
             file_put_contents(__DIR__.'/destruct.log',$_SERVER['REMOTE_ADDR'].' destruct_end_'.time()."\n",FILE_APPEND);
         }*/
        if($this->connectionStatus == true) {
            pg_close($this->link);
        }
    }

}