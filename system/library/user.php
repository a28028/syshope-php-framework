<?php

/**
 * @property DB $db
 */
class User
{

    private $user_id;
    private $getUserIP;
    private $username;
    private $permission = array();

    public function __construct($registry)
    {
        $this->db = $registry->get('db');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');
        $this->getUserIP = $this->getClientIP();
        if (isset($this->session->data['user_id'])) {
            //$user_query = $this->db->query("SELECT * FROM " .$this->db->getTable('users') . "  WHERE user_id = '" . (int)$this->session->data['user_id'] . "' AND status = '1'");
            $user_query = $this->db->query("SELECT * FROM " . $this->db->getTable('users') . "  WHERE user_id = '" . (int)$this->session->data['user_id'] . "' AND status != '0'");

            if ($user_query->num_rows) {
                $this->user_id = $user_query->row['user_id'];
                $this->username = $user_query->row['username'];
                $this->user_group_id = $user_query->row['user_group_id'];
                $this->index_code_id = $user_query->row['index_code_id'];
                $this->email = $user_query->row['email'];
                $this->status = $user_query->row['status'];

                $this->db->query("UPDATE " . $this->db->getTable('users') . "  SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE user_id = '" . (int)$this->session->data['user_id'] . "'");

                $user_group_query = $this->db->query("SELECT permission FROM " . $this->db->getTable('groups') . " WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

                $permissions = unserialize($user_group_query->row['permission']);

                if (is_array($permissions)) {
                    foreach ($permissions as $key => $value) {
                        $this->permission[$key] = $value;
                    }
                }
            } else {
                $this->logout();
            }
        }
    }

    public function getClientIP()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    public function login($username, $password)
    {
        //   $user_query = $this->db->query("SELECT * FROM " . $this->db->getTable('users'). " WHERE username = '" . $this->db->escape($username) . "' AND (user_password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR user_password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
        $where = " username = '" . $this->db->escape($username) . "' AND (user_password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR user_password = '" . $this->db->escape(md5($password)) . "') AND  (status = '1' or status = '-2' OR status = '-1' )";
        $where .= "OR ( username = '" . $this->db->escape($username) . "' AND   (user_password = '" . $this->db->escape(md5($password)) . "') AND (status = '-2' OR status = '-1' ))";
        $user_query = $this->db->query("SELECT * FROM " . $this->db->getTable('users') . " WHERE " . $where);
        //$user_query = $this->db->query("SELECT * FROM " . $this->db->getTable('users'). " WHERE username = '" . $this->db->escape($username) . "' AND (user_password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR user_password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
        if ($user_query->num_rows) {
            $this->session->data['user_id'] = $user_query->row['user_id'];

            $this->user_id = $user_query->row['user_id'];
            $this->username = $user_query->row['username'];
            $this->user_group_id = $user_query->row['user_group_id'];
            $this->index_code_id = $user_query->row['index_code_id'];
            $this->status = $user_query->row['status'];
            $this->email = $user_query->row['email'];

            $user_group_query = $this->db->query("SELECT permission FROM " . $this->db->getTable('groups') . " WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

            $permissions = unserialize($user_group_query->row['permission']);
            if (is_array($permissions)) {
                foreach ($permissions as $key => $value) {
                    $this->permission[$key] = $value;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function logout()
    {
        unset($this->session->data['user_id']);
        $this->user_id = '';
        $this->username = '';
    }

    public function hasPermission($key, $value)
    {
        if (isset($this->permission[$key])) {
            return in_array($value, $this->permission[$key]);
        } else {
            return false;
        }
    }

    public function isLogged()
    {
        return $this->user_id;
    }

    public function getId()
    {
        return $this->user_id;
    }

    public function getUserIP()
    {
        return $this->getUserIP;
    }

    public function getUserName()
    {
        return $this->username;
    }

    public function getGroupId()
    {
        return $this->user_group_id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function is_admin()
    {
        return $this->getUserName() == 'admin';
    }

    public function getIndexCodeId()
    {
        return $this->index_code_id;
    }
}