<?php

class Url
{
    private $domain;
    private $ssl;
    private $rewrite = array();
    private $routeName = 'route';
    private $token = false;

    public function __construct($domain, $ssl = '')
    {
        $this->domain = $domain;
        $this->ssl = $ssl;
    }

    public function addRewrite($rewrite)
    {
        $this->rewrite[] = $rewrite;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function setRouteName($routeName)
    {
        $this->routeName = $routeName;
    }

    public function link($route, $args = '', $secure = false)
    {
        if (!$secure) {
            $url = $this->domain;
        } else {
            $url = $this->ssl;
        }

        $url .= 'index.php?' . $this->routeName . '=' . $route;

        if ($args) {
            $url .= str_replace('&', '&amp;', '&' . ltrim($args, '&'));
        }

        foreach ($this->rewrite as $rewrite) {
            $url = $rewrite->rewrite($url);
        }

        return $url;
    }

    public function linkTokenSsl($route, $args = '')
    {
        if ($this->token) {
            if ($args == '') {
                $args .= "token=" . $this->token;
            } else {
                $args .= "&token=" . $this->token;
            }
        }

        $secure = 'ssl';
        if (!$secure) {
            $url = $this->domain;
        } else {
            $url = $this->ssl;
        }

        $url .= 'index.php?' . $this->routeName . '=' . $route;

        if ($args) {
            $url .= str_replace('&', '&amp;', '&' . ltrim($args, '&'));
        }

        foreach ($this->rewrite as $rewrite) {
            $url = $rewrite->rewrite($url);
        }

        return $url;
    }
}
