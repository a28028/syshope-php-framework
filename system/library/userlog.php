<?php

/**
 * Class Userlog
 */
class Userlog {
    /**
     * @var DB $db
     */
    private $db,
        $config;
    /**
     * @var SsoUser $user
     */
    private $user;

    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->db = new DB(DB_DRIVER_LOG, DB_HOSTNAME_LOG, DB_USERNAME_LOG, DB_PASSWORD_LOG, DB_DATABASE_LOG);
        $this->user = $registry->get('user');
    }

    public function write($action_type, $action_desc = '', $params = false) {
        if (!preg_match('/[^A-Za-z0-9]/', $action_type)) // '/[^a-z\d]/i' should also work.
        {
            throw new InvalidDataException("action_type string contains only english letters & digits");
            // 
        }
        $implodeValue = array();
        $implodeColumns = array();
        if ($this->user->isLogged()) {
            $implodeColumns [] = 'username';
            $implodeValue[] = "'" . $this->user->getUserName() . "'";
        }
        else{
            $implodeColumns [] = 'username';
            $implodeValue[] = "'anonymous'";

        }
        $implodeColumns [] = 'system_code';
        $implodeValue[] = SYSTEM_CODE_LOG;
        $implodeColumns [] = 'action_date';
        $implodeValue[] = 'now()';
        if (!empty($action_desc)) {
            $implodeColumns [] = 'action_desc';
            $implodeValue[] = "'" . $this->db->escape($action_desc) . "'";
        }
        $implodeColumns [] = 'action_type';
        $implodeValue[] = "'" . $this->db->escape($action_type) . "'";
        if ($params !== false) {
            if (array_key_exists('password', $params)) {
                unset($params['password']);
            }
            $implodeColumns [] = 'params';
            $implodeValue[] = "'" . $this->db->escape(json_encode($params)) . "'";
        }
        $ip_addr = $this->getClientIP();
       // $ip_addr = '192.168.200.56, 178.236.38.37';
        $ip_addr_array = explode(',',$ip_addr);
        if(count($ip_addr_array) == 2){
            $ip_addr = $ip_addr_array[1];
        }
        else{
            $ip_addr = $ip_addr_array[0];
        }
        $implodeColumns [] = 'ip_addr';
        $implodeValue[] = "'" . trim ($this->db->escape($ip_addr)) . "'";
        $sql = "insert into " . $this->getTable('user_logs') . " ";
        $sql .= " (" . implode(",", $implodeColumns) . ") ";
        $sql .= "VALUES (" . implode(",", $implodeValue) . ") ";
        $sql .= "RETURNING id";
        $query = $this->db->query($sql);
        $user_id = $query->row['id'];
        return $user_id;
    }

    private function getClientIP() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    private function getTable($tableName) {
        return DB_SCHEMA_LOG . "." . DB_PREFIX_LOG . $tableName;
    }
}

class InvalidDataException extends \Exception {
}
