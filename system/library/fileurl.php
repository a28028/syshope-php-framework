<?php

class FileUrl 
{
    protected $registry;

    public function __construct($registry) {
        $this->registry = $registry;
    }

    public function __get($key) {
        return $this->registry->get($key);
    }

    public function __set($key, $value) {
        $this->registry->set($key, $value);
    }

    /**
     * @param $filename 'image/1.jpg'
     * @param $width
     * @param $height
     * @return string|void
     */
    public function resizeImgLink($filename, $width, $height)
    {
        $filename = trim($filename);
        if(strrpos($filename, DIR_SEP) === 0){
            $filename = substr($filename,1,strlen($filename) - 1);
        }
        if (!is_file(DIR_STORAGE . $filename)) {
            return;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $old_image = $filename;
        $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (!is_file(DIR_STORAGE . $new_image) || (filectime(DIR_STORAGE . $old_image) > filectime(DIR_STORAGE . $new_image))) {
            $path = '';

            $directories = explode('/', dirname(str_replace('../', '', $new_image)));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir(DIR_STORAGE . $path)) {
                    @mkdir(DIR_STORAGE . $path, 0777);
                }
            }

            list($width_orig, $height_orig) = getimagesize(DIR_STORAGE . $old_image);

            if ($width_orig != $width || $height_orig != $height) {
                $image = new Image(DIR_STORAGE . $old_image);
                $image->resize($width, $height);
                $image->save(DIR_STORAGE . $new_image);
            } else {
                copy(DIR_STORAGE . $old_image, DIR_STORAGE . $new_image);
            }

        }

        if ($this->request->server['HTTPS']) {
            return $this->config->get('config_ssl') . $this->config->get('config_storage') . '/' . $new_image;
        } else {
            return $this->config->get('config_url') . $this->config->get('config_storage') . '/' . $new_image;
        }
      //  echo DIR_STORAGE . $filename;return;
    }

    public function link($filename)
    {

        if ($this->request->server['HTTPS']) {
            return $this->config->get('config_ssl') . $this->config->get('config_storage') . '/' . $filename;
        } else {
            return $this->config->get('config_url') . $this->config->get('config_storage') . '/' . $filename;
        }
    }

    public function getUpload($dirStorage , $base_dir_name = '', $_files_Key = 'file' , $config_file_ext_allowed = '' ,$config_file_mime_allowed = '')
    {
        //ini_set('display_errors', '1');
        $this->load->helper('utf8');

        // $dirStorage = DIR_STORAGE;
        // $dirStorage = DIR_STORAGE_VENDOR_APP;
        //  $base_dir_name = 'register';
        //  $this->load->model('member/register');
        if(strlen($config_file_ext_allowed) == 0){
            $config_file_ext_allowed = $this->config->get('config_file_ext_allowed');
        }
        if(strlen($config_file_mime_allowed) == 0){
            $config_file_mime_allowed = $this->config->get('config_file_mime_allowed');
        }

        $resultData = array();

        if (!empty($_FILES[$_files_Key]['name']) && is_file($_FILES[$_files_Key]['tmp_name'])) {
            // Sanitize the filename
            $filename = basename(preg_replace('/[^a-zA-Z0-9\.\-\s+]/', '', html_entity_decode($_FILES[$_files_Key]['name'], ENT_QUOTES, 'UTF-8')));
            // Validate the filename length
            if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
                $resultData['error_warning'] = 'نام فایل معتبر نمی باشد';
                return $resultData;
            }
            // Allowed file extension types
            $allowed = array();
            $extension_allowed = preg_replace('~\r?\n~', "\n", $config_file_ext_allowed);
            $filetypes = explode("\n", $extension_allowed);
            foreach ($filetypes as $filetype) {
                $allowed[$filetype] = trim($filetype);
            }
            if (!array_key_exists(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
                // $json['error_warning'] = 'فایل نامعتبر می باشد';
                $resultData['error_warning'] = 'نام فایل معتبر نمی باشد';
                return $resultData;
            }
            // Allowed file mime types
            $allowed = array();

            $mime_allowed = preg_replace('~\r?\n~', "\n", $config_file_mime_allowed);
            $filetypes = explode("\n", $mime_allowed);
            foreach ($filetypes as $filetype) {
                $allowed[$filetype] = trim($filetype);
            }
            
            if (!array_key_exists($_FILES[$_files_Key]['type'], $allowed)) {
                /*  print_r($allowed);
                  print_r($_FILES[$_files_Key]['type']);*/
                // $json['error_warning'] = 'فایل نامعتبر می باشد';//'error_filetype';
                $resultData['error_warning'] = 'نوع فایل معتبر نمی باشد';
            /*    echo $_FILES[$_files_Key]['type'];
                var_dump($allowed);
                echo 5555555555;exit;*/
                return $resultData;
            }
            // Check to see if any PHP files are trying to be uploaded
            $content = file_get_contents($_FILES[$_files_Key]['tmp_name']);
            if (preg_match('/\<\?php/i', $content)) {
                $resultData['error_warning'] = 'فایل نامعتبر می باشد';//'error_filetype';
                return $resultData;
            }
            // Return any upload error
            if ($_FILES[$_files_Key]['error'] != UPLOAD_ERR_OK) {
                // $json['error_warning'] = 'خطا error_upload=>' . $_FILES[$_files_Key]['error'];
                $this->response->setOutput_warning_message('خطا error_upload=>' . $_FILES[$_files_Key]['error']);
                $resultData['error_warning'] = 'خطا   !UPLOAD_ERR_OK=>' . $_FILES[$_files_Key]['error'];//'error_filetype';
                return $resultData;
            }
        } else {
            $resultData['error_warning'] = 'خطا در ارسال فایل';
            return $resultData;
            //  $json['error_warning'] = 'error_upload';
        }

        if (!$resultData) {
            if (!is_dir($dirStorage )) {
                mkdir($dirStorage );
            }
            if (!is_dir($dirStorage.$base_dir_name.DIRECTORY_SEPARATOR)) {
                mkdir($dirStorage.$base_dir_name.DIRECTORY_SEPARATOR);
            }
            // $target_file = DIR_STORAGE .DIRECTORY_SEPARATOR.basename($_FILES["file"]["name"]);
            $path = $_FILES[$_files_Key]['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $baseName = pathinfo($path, PATHINFO_BASENAME);
            //$target_file = DIR_STORAGE . DIRECTORY_SEPARATOR . $key . "." . $ext;
            $token = md5(mt_rand());
            $attachment_path = $base_dir_name. DIRECTORY_SEPARATOR . $token . '_' . $baseName;
            $target_file = $dirStorage.$attachment_path;
            //   echo $target_file;exit;
            while (file_exists($target_file)) {
                $token = md5(mt_rand());
                $attachment_path = $base_dir_name. DIRECTORY_SEPARATOR . $token . '_' . $baseName;
                $target_file = $dirStorage.$attachment_path;
            }
            move_uploaded_file($_FILES[$_files_Key]['tmp_name'], $target_file);
            $resultData = array(
                "attachment_path" => $attachment_path,
                "file_name" => $baseName,
                "file_extension" => strtolower(substr(strrchr($filename, '.'), 1)),
                "file_size" => $_FILES[$_files_Key]['size'],
                "file_type" => $_FILES[$_files_Key]['type'],
            );
            return $resultData;
        }

    }

    public function delete_attachment_path($attachment_path , $dirStorage = ''){
        if(strlen($dirStorage) == 0){
            $dirStorage = DIR_STORAGE;
        }
        $filename = trim($attachment_path);
        if(strrpos($filename, DIR_SEP) === 0){
            $attachment_path = substr($attachment_path,1,strlen($attachment_path) - 1);
        }
        if (is_file($dirStorage . $attachment_path)) {
            unlink($dirStorage . $attachment_path);
            return true;
        }
        return false;
    }

}