<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="<?= $active('common/home/index') ?>"><a
                            href="<?= $url->link('common/home/index'); ?>"><?= $lg['home'] ?></a></li>
                <li class="<?= $active('common/home/about') ?>"><a
                            href="<?= $url->link('common/home/about'); ?>"><?= $lg['about'] ?></a></li>
                <li class="<?= $active('common/home/contact') ?>"><a
                            href="<?= $url->link('common/home/contact'); ?>"><?= $lg['contact'] ?></a>
                </li>
                <li class="<?= $active('common/blog/index') ?>"><a
                            href="<?= $url->link('common/blog/index'); ?>"><?= $lg['blog'] ?></a></li>
                <li class="<?= $active('common/blog/blogTwig') ?>"><a
                            href="<?= $url->link('common/blog/blogTwig'); ?>"><?= $lg['blog_twig'] ?></a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?= $lg['layout'] ?>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a target="_blank"
                               href="index.php?road=common/codemirror/template&path=layouts/default/header.tpl">header.tpl</a>
                        </li>
                        <li><a target="_blank"
                               href="index.php?road=common/codemirror/template&path=layouts/default/navbar.tpl">navbar.tpl</a>
                        </li>
                        <li><a target="_blank"
                               href="index.php?road=common/codemirror/template&path=layouts/default/index.tpl">index.tpl</a>
                        </li>
                        <li><a target="_blank"
                               href="index.php?road=common/codemirror/template&path=layouts/default/footer.tpl">footer.tpl</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">docs
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a class="<?= $active('docs/index/mvc') ?>"
                               href="<?= $url->link('docs/index/mvc'); ?>">mvc architecture</a>
                        </li>
                        <li><a class="<?= $active('docs/index/multiLanguage') ?>"
                               href="<?= $url->link('docs/index/multiLanguage'); ?>"><?= $lg['multi_language'] ?></a>
                        </li>
                        <li><a class="<?= $active('docs/index/database') ?>"
                               href="<?= $url->link('docs/index/database'); ?>">database</a>
                        </li>
                    </ul>
                </li>
            </ul>


        </div><!--/.nav-collapse -->

    </div>
</nav>
<?php echo $language; ?>