<footer class="bs-docs-footer">
    <div class="container">
        <p>Code licensed <a href="https://github.com/twbs/bootstrap/blob/master/LICENSE" target="_blank" rel="license">MIT</a>
    </div>
</footer>

<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo $base; ?>view/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo $base; ?>view/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo $base; ?>view/layouts/default/assets/js/ie10-viewport-bug-workaround.js"></script>
<script type="application/javascript">
    $(document).ready(function() {
        // Language
        $('#language ul a').on('click', function (e) {
            e.preventDefault();

            $('#language input[name=\'code\']').attr('value', $(this).attr('href'));

            $('#language').submit();
        });
    });
</script>
</body>
</html>