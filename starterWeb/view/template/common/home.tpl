<div class="panel panel-default" style="text-align: center">
    <div class="panel-heading">
        <h1>Welcome to syshope!</h1>
    </div>
    <div class="panel-body">
        <div class="well">
            <p>The page you are looking at is being generated dynamically by syshope.</p>

            <p>If you would like to edit this page you'll find it located at:</p>
            <code>view/template/common/home.tpl</code>
            <a target="_blank" href="index.php?road=common/codemirror/template&path=template/common/home.tpl">view source</a>
            <p>The corresponding controller for this page is found at:</p>
            <code>controller/common/home.php - function index()</code>
            <a target="_blank" href="index.php?road=common/codemirror/template&path=controller/common/home.php">view source</a>

        </div>
    </div>
</div>