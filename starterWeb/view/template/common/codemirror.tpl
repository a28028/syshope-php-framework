<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo $base; ?>view/bower_components/codemirror/lib/codemirror.css" rel="stylesheet">
    <link href="<?php echo $base; ?>view/bower_components/codemirror/theme/monokai.css" rel="stylesheet">
    <script src="<?php echo $base; ?>view/bower_components/codemirror/lib/codemirror.js"></script>
    <script src="<?php echo $base; ?>view/bower_components/codemirror/mode/xml/xml.js"></script>
    <script src="<?php echo $base; ?>view/bower_components/codemirror/mode/clike/clike.js"></script>
    <script src="<?php echo $base; ?>view/bower_components/codemirror/mode/php/php.js"></script>

    <![endif]-->
    <style>
        .CodeMirror {
            border: 1px solid #eee;
            height: auto;
        }
    </style>
</head>
<body class="">
<h3>filename {<?=$path ?>}</h3>
<textarea name="code" id="code" rows="10" style="display: none;"></textarea>
<script type="application/javascript">
    var codemirror = CodeMirror.fromTextArea(document.getElementById("code"), {
        mode: '<?=$mode?>',
        height: '700px',
        lineNumbers: true,
        autofocus: true,
        theme: 'monokai',
        readOnly : true
    });
    var json = <?=$json ?>;
    codemirror.setValue(json.html);

</script>
</body>
</html>


