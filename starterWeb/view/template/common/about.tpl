<div class="panel panel-default">
    <div class="panel-heading">
        <h1>Welcome to about!</h1>
    </div>
    <div class="panel-body">
        <div class="well">
            <p>The page you are looking at is being generated dynamically by syshope.</p>
            <p>If you would like to edit this page you'll find it located at:</p>
            <code>view/template/common/about.tpl</code>
            <a target="_blank" href="index.php?road=common/codemirror/template&path=template/common/about.tpl">view source</a>
            <p>The corresponding controller for this page is found at:</p>
            <code>controller/common/home.php - function about()</code>
            <a target="_blank" href="index.php?road=common/codemirror/template&path=controller/common/home.php">view source</a>
        </div>
    </div>
</div>