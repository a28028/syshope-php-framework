<div class="panel panel-default">
    <div class="panel-heading">
        <h1>Welcome to blog!</h1>
    </div>
    <div class="panel-body">
        <div class="well">
            <p>The page you are looking at is being generated dynamically by syshope.</p>

            <p>If you would like to edit this page you'll find it located at:</p>
            <code>view/template/common/blog.tpl</code>
            <a target="_blank" href="index.php?road=common/codemirror/template&path=template/common/blog.tpl">view source</a>
            <p>The corresponding controller for this page is found at:</p>
            <code>controller/common/blog.php - function blog()</code>
            <a target="_blank" href="index.php?road=common/codemirror/template&path=controller/common/blog.php">view source</a>

        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3><?= $blog_heading ?></h3>
    </div>
    <div class="panel-body">
        <div class="well">
            <?php foreach ($blog_entries as $item) { ?>
                <h5><?= $item['title'] ?></h5>
                <p><?= $item['body'] ?></p>
            <?php } ?>
        </div>
    </div>
</div>

