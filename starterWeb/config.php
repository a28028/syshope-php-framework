<?php
// HTTP
//define('HTTP_SERVER', 'http://website.com/starterWeb/');
$subUrlDir = rtrim(rtrim(str_replace( basename($_SERVER['SCRIPT_NAME']), '',$_SERVER['SCRIPT_NAME']), '//'), '/').'/';
define('HTTP_SERVER',  "http://{$_SERVER['HTTP_HOST']}".$subUrlDir);
error_reporting(E_ALL);
ini_set('display_errors', 1);
define('DIR_SEP',DIRECTORY_SEPARATOR);
// HTTPS
//define('HTTP_SERVER', 'https://website.com/starterWeb/');
define('HTTPS_SERVER', HTTP_SERVER);
// DIR
define('DIR_APPLICATION',__DIR__.DIR_SEP);
define('DIR_SYSTEM',realpath( DIR_APPLICATION.'..'.DIR_SEP.'system').DIR_SEP);
define('DIR_LANGUAGE', DIR_APPLICATION.'language/');
define('DIR_TEMPLATE', DIR_APPLICATION.'view/');
define('DIR_ANG_TEMPLATE', DIR_APPLICATION.'..'.DIR_SEP.'ui/views'.DIR_SEP);
define('DIR_CONFIG', DIR_SYSTEM.'config/');
define('STORAGE','storage');
define('DIR_STORAGE', DIR_APPLICATION.DIR_SEP.STORAGE.DIR_SEP);
define('DIR_SYSTEM_STORAGE', DIR_SYSTEM.DIR_SEP.'storage'.DIR_SEP);
define('DIR_CACHE', DIR_SYSTEM.'cache'.DIR_SEP);
define('DIR_DOWNLOAD', DIR_SYSTEM.'download'.DIR_SEP);
define('DIR_UPLOAD',  DIR_SYSTEM.'upload'.DIR_SEP);
define('DIR_LOGS', DIR_SYSTEM.'logs'.DIR_SEP);
define('DIR_MODIFICATION', DIR_SYSTEM.'modification'.DIR_SEP);
define("DEFAULT_LAYOUT_CONTENT", 'layouts/default/');

// DB
define('DB_DRIVER', 'sqlite');
define('DB_HOSTNAME', DIR_STORAGE.'identifier.sqlite');
/*define('DB_USERNAME', 'root');
define('DB_PASSWORD', '12345678');
define('DB_DATABASE', 'TctWebServices');*/
define('DB_PREFIX', 'oc_');



