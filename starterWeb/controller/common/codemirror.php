<?php

class ControllerCommonCodemirror extends Controller
{
    public function template()
    {
        $data = array();
        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }
        $data['base'] = $server;
        $json = new stdClass();
        $json->html = '';
        if (isset($this->request->get['path'])) {
            $path = $this->request->get['path'];
        } else {
            $path = '';
        }
        $file = '';
        $extension = pathinfo($path)['extension'];
        switch ($extension) {
            case  'tpl':
            case  'twig':
                $file = DIR_TEMPLATE . $path;
                $data['mode']= 'text/html';
                break;
            case  'php':
                $file = DIR_APPLICATION . $path;
                $data['mode']= 'application/x-httpd-php';
                break;
        }


        if (is_file($file)) {
            $json->html = file_get_contents($file);
        }
        $data['json'] = json_encode($json);
        $data['path'] = $path;

        $this->response->setOutput($this->load->view('template/common/codemirror.tpl', $data));

    }
}
