<?php
class ControllerCommonHome extends Controller {
	public function index() {
        $contentController = $this->load->view('template/common/home.tpl');
        $this->layout->showContent($contentController);
	}
	public function about(){
        $contentController = $this->load->view('template/common/about.tpl');
        $this->layout->showContent($contentController);
    }
    public function contact(){
        $contentController = $this->load->view('template/common/contact.tpl');
        $this->layout->showContent($contentController);
    }
}