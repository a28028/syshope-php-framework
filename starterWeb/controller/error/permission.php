<?php

class ControllerErrorPermission extends Controller
{
    public function index()
    {
        $this->response->addHeader('Content-Type: text/html');
        $this->response->setOutput($this->load->angView('error/permission.tpl'));
    }

    public function check()
    {
        if (isset($this->request->get['route'])) {
            $route = '';

            $part = explode('/', $this->request->get['route']);

            if (isset($part[0])) {
                $route .= $part[0];
            }

            if (isset($part[1])) {
                $route .= '/' . $part[1];
            }

            $ignore = array(
                'common/dashboard',
                'common/login',
                'common/logout',
                'common/forgotten',
                'common/reset',
                'error/not_found',
                'error/permission'
            );
            if (!in_array($route, $ignore) && !$this->user->hasPermission('access', $route)) {
                return new Action('error/permission');
            }
        }
    }

    public function indexJson()
    {
        if (isset($this->request->get['route'])) {
            $route = '';

            $part = explode('/', $this->request->get['route']);

            if (isset($part[0])) {
                $route .= $part[0];
            }

            if (isset($part[1])) {
                $route .= '/' . $part[1];
            }
            $ignore = array(
                'common/dashboard',
                'common/login',
                'common/logout',
                'common/forgotten',
                'common/reset',
                'error/not_found',
                'error/permission'
            );
            if (!in_array($route, $ignore) && !$this->user->hasPermission('access', $route)) {
                $json = array();
                $json['error'] = 'error_permission';
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
            }
        }
    }
}