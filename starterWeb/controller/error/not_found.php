<?php
class ControllerErrorNotFound extends Controller {
	public function index() {
        $contentController = $this->load->view('default/template/error/not_found.tpl');
        $this->document->setTitle("error 404 ") ;
        $this->layout->showContent($contentController);
	}
}