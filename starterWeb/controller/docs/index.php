<?php

/**
 * Class ControllerDocsIndex
 * @property ModelDocsIndex $model_docs_index
 */
class ControllerDocsIndex extends Controller
{
    public function mvc()
    {
        $contentController = $this->load->view('template/docs/mvc.twig');
        $this->layout->showContent($contentController);
    }

    public function multiLanguage()
    {
        $data = array();
        $data['lg'] = $this->language->load('docs/language');
        $contentController = $this->load->view('template/docs/language.twig', $data);
        $this->layout->showContent($contentController);
    }

    public function database()
    {
        $data = array();
        $this->load->model('docs/index');
        if ($this->request->post) {
            $this->model_docs_index->exampleSave($this->request->post);
            $data['success'] = 'This alert box indicates a successful or positive action.';
        }
        $data['examplelist'] = $this->model_docs_index->examplelist();
        $data['lg'] = $this->language->load('docs/database');
        $contentController = $this->load->view('template/docs/database.twig', $data);
        $this->layout->showContent($contentController);

    }
}