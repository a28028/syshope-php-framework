<?php

class ControllerlayoutsDefault extends Controller
{
    public function index($args = array())
    {
        $contentController = $args['contentController'];
        $this->document->setTitle($this->config->get('config_meta_title') . $this->document->getTitle());
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));
        $route_name = $this->config->get('route_name');
        if (isset($this->request->get[$route_name])) {
            $this->document->addLink(HTTP_SERVER, 'canonical');
        }
        $data['header'] = $this->header();
        $data['navbar'] = $this->navbar();
        $data['content'] = $contentController;
        $data['footer'] = $this->footer();
        $this->response->setOutput($this->load->view('layouts/default/index.tpl', $data));
    }

    private function header()
    {
        $data['title'] = $this->document->getTitle();
        $data['direction'] = $this->language->get('direction');
        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }
        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts();
        //$data['lang'] = $this->language->get('code');
        //$data['direction'] = $this->language->get('direction');
        //$data['google_analytics'] = html_entity_decode($this->config->get('config_google_analytics'), ENT_QUOTES, 'UTF-8');
        $data['name'] = $this->config->get('config_name');
        $data['icon'] = '';
        /*if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $data['icon'] = $server . 'image/' . $this->config->get('config_icon');
        } else {
            $data['icon'] = '';
        }*/

        /*if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }*/
        $data['logo'] = '';
        $data['text_home'] = 'home';
        $data['home'] = $this->url->link('common/home');
        //$data['logged'] = $this->customer->isLogged();
        $data['contact'] = $this->url->link('information/contact');
        $data['telephone'] = $this->config->get('config_telephone');
        $status = true;
        if (isset($this->request->server['HTTP_USER_AGENT'])) {
            $robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

            foreach ($robots as $robot) {
                if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
                    $status = false;

                    break;
                }
            }
        }
        return $this->load->view('layouts/default/header.tpl', $data);
    }

    private function navbar()
    {
        $data = array();
        $data['language'] = $this->languageIndex();

        $data['active'] = function ($linkRoad) {
            $road = '';
            if (isset($_GET['road'])) {
                $road = $_GET['road'];
                $road = htmlspecialchars($road, ENT_COMPAT, 'UTF-8');
            }
            return $road == $linkRoad ? 'active' : '';
        };
        $data['lg'] =  $this->language->load('layouts/default/navbar');
        return $this->load->view('layouts/default/navbar.tpl', $data);
    }

    private function footer()
    {
        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }
        $data = array();
        $data['base'] = $server;
        $data['powered'] = sprintf('text_powered', 'config_name', date('Y', time()));
        return $this->load->view('layouts/default/footer.tpl', $data);
    }

    public function languageIndex()
    {
        $data['text_language'] = $this->language->get('text_language');

        $data['action'] = $this->url->link('layouts/default/language', '', $this->request->server['HTTPS']);

        $data['code'] = $this->session->data['language'];

        $data['languages'] = array();

        $results = $this->language->getLanguages();

        foreach ($results as $result) {
            if ($result['status']) {
                $data['languages'][] = array(
                    'name' => $result['name'],
                    'code' => $result['code'],
                    'image' => $result['image']
                );
            }
        }
        $route_name = $this->config->get('route_name');
        if (!isset($this->request->get[$route_name])) {
            $data['redirect'] = $this->url->link('common/home');
        } else {
            $url_data = $this->request->get;

            unset($url_data['_route_']);

            $route = $url_data[$route_name];

            unset($url_data[$route_name]);

            $url = '';

            if ($url_data) {
                $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            }

            $data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
        }
        return $this->load->view('layouts/default/language.tpl', $data);

    }

    public function language()
    {
        if (isset($this->request->post['code'])) {
            $this->session->data['language'] = $this->request->post['code'];
        }

        if (isset($this->request->post['redirect'])) {
            $this->response->redirect($this->request->post['redirect']);
        } else {
            $this->response->redirect($this->url->link('common/home'));
        }
    }
}