<?php
// Version
define('VERSION', '1.0.0.1');

// Configuration
if (is_file('config.php')) {
    require_once('config.php');
}

// Install
/*if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}*/

// Startup
require_once(DIR_SYSTEM . 'startup.php');

// Registry
$registry = new Registry();

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

// Config
// Config
$config = new Config();
$config->set('config_store_id', "2");
$config->set('config_error_filename', "error.log");
$config->set('config_error_display', "1");
$config->set('config_compression', "0");
$config->set('config_encryption', "f5cb9f90a63fb7373fc9195c63bb47b8");
$config->set('config_url', HTTP_SERVER);
$config->set('config_ssl', HTTPS_SERVER);
$config->set('default_layout_content', DEFAULT_LAYOUT_CONTENT);
$config->set('config_template', 'default');
$config->set('route_name', 'road');
$config->set('config_meta_title', 'sysHope ');
$config->set('config_language', 'en-gb');
$config->set('config_file_ext_allowed',
    "txt
png
jpe
jpeg
jpg
gif
bmp
ico
tiff
tif
svg
svgz
zip
rar
msi
cab
mp3
qt
mov
pdf
psd
ai
eps
ps
doc
rtf
xls
ppt
odt
ods");
$config->set('config_file_mime_allowed',
    "text/plain
image/png
image/jpeg
image/gif
image/bmp
image/vnd.microsoft.icon
image/tiff
image/svg+xml
application/zip
application/x-rar-compressed
application/x-msdownload
application/vnd.ms-cab-compressed
audio/mpeg
video/quicktime
application/pdf
image/vnd.adobe.photoshop
application/postscript
application/msword
application/rtf
application/vnd.ms-excel
application/vnd.ms-powerpoint
application/vnd.oasis.opendocument.text
application/vnd.oasis.opendocument.spreadsheet");
$config->set('config_storage', STORAGE);
$registry->set('config', $config);
// Database
$db = new DB(DB_DRIVER, DB_HOSTNAME, '', '', '');
$registry->set('db', $db);

$layout = new Layout($registry);
$layout->setDefaultLayoutRoute($config->get('default_layout_content'));
$registry->set('layout', $layout);
// Request
$request = new Request();
$registry->set('request', $request);

// Url
$url = new Url($config->get('config_url'), $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url'));
$url->setRouteName($config->get('route_name'));
$registry->set('url', $url);

// Log
$log = new Log($config->get('config_error_filename'));
$registry->set('log', $log);

function error_handler($errno, $errstr, $errfile, $errline)
{
    global $log, $config;

    // error suppressed with @
    if (error_reporting() === 0) {
        return false;
    }

    switch ($errno) {
        case E_NOTICE:
        case E_USER_NOTICE:
            $error = 'Notice';
            break;
        case E_WARNING:
        case E_USER_WARNING:
            $error = 'Warning';
            break;
        case E_ERROR:
        case E_USER_ERROR:
            $error = 'Fatal Error';
            break;
        default:
            $error = 'Unknown';
            break;
    }

    if ($config->get('config_error_display')) {
        echo '<b>' . $error . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';
    }

    if ($config->get('config_error_log')) {
        $log->write('PHP ' . $error . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
    }

    return true;
}

// Error Handler
set_error_handler('error_handler');

// Response
$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$response->setCompression($config->get('config_compression'));
$registry->set('response', $response);

// Cache
$cache = new Cache('file');
$registry->set('cache', $cache);

// Session
$session = new Session();
$registry->set('session', $session);
// Language
require_once(DIR_SYSTEM . 'startup/language.php');

// Document
$registry->set('document', new Document());


// Encryption
$registry->set('encryption', new Encryption($config->get('config_encryption')));

// Event
$event = new Event($registry);
$registry->set('event', $event);

/*$query = $db->query("SELECT * FROM " . DB_PREFIX . "event");

foreach ($query->rows as $result) {
	$event->register($result['trigger'], $result['action']);
}*/

// Front Controller
$controller = new Front($registry);

// Maintenance Mode
//$controller->addPreAction(new Action('common/maintenance'));

// SEO URL's
//$controller->addPreAction(new Action('common/seo_url'));

// Router
if (isset($request->get[$config->get('route_name')])) {
    $action = new Action($request->get[$config->get('route_name')]);
} else {
    $action = new Action('common/home');
}

// Dispatch
$controller->dispatch($action, new Action('error/not_found'));

// Output
$response->output();
